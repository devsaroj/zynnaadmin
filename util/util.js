let moment = require("moment");
let jwt = require('jsonwebtoken');
let config = require('../config/config');
let messageConf = require('../constants/messages');
let fs = require('fs');
let nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
    host: 'mail.zynna.in',
    port: 587,
    secure: false,
    tls: {
        rejectUnauthorized: false
    },
    auth: {
        user: 'internal@zynna.in',
        pass: 'asdf@856$#'
    }
});
let crypto = require('crypto');
let fileName = 'error.txt'

let dateUtil = {
    /* Add All the date utils here */
};
let appUtil = {
    /* Add all app level util here */
    generateId: () => {
        let randNumber = Math.floor((Math.random() * 100) + 1);
        let currentDate = new Date().getTime();
        return "MT" + randNumber + "" + currentDate;
    },
    signToken: (data) => {
        /* Create signed token */
        return jwt.sign(data, config.tokenCode, {expiresIn: '365d'})
    },
    verifyToken: (token) => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, config.tokenCode, function (err, decoded) {
                if (err)
                    return reject(err);
                return resolve(decoded);
            });
        });
    },
    empty: (data) => {
        return (typeof data === "undefined" || data === "");
    }, isNull: (data) => {
        return (data === null);
    }, sendMail: (mailOptions) => {
        return new Promise((resolve, reject) => {
            transporter.sendMail(mailOptions, function (error, info) {
                let emailSent = {
                    name: mailOptions.to,
                    message: ''
                }
                if (error) {
                    emailSent.message = error
                    fs.appendFile(fileName, new Date().toLocaleDateString() + ': ' + JSON.stringify(emailSent) + "\n" || '' + "\n", function (errorR) {
                        if (errorR) throw errorR;
                    });
                    return reject(error);
                } else {
                    emailSent.message = 'email sent successfully'
                    fs.appendFile(fileName, new Date().toLocaleDateString() + ': ' + JSON.stringify(emailSent) + "\n" || '' + "\n", function (errorR) {
                        if (errorR) throw errorR;
                    });
                    return resolve();
                }
            });
        });
    }, generateRandomToken: () => {
        return new Promise((resolve, reject) => {
            crypto.randomBytes(20, function (err, buffer) {
                if (err) return reject(err);
                let token = buffer.toString('hex');
                return resolve(token)
            });
        });
    }, generatePassword: function () {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < 6; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }, validateObject(data) {
        let finalString = ''
        if (typeof data != "undefined" && data != null && data != '') {
            if (typeof data != "object") {
                finalString = data.trim().toUpperCase();
            } else {
                if (typeof data.text != "undefined" && data.text != null && data.text != '') {
                    finalString = data.text.trim().toUpperCase();
                } else {
                    finalString = ''
                }
            }
        }
        return finalString;
    }
};

let finalResponse = {
    success: (body = true, message) => {
        return {
            status: 1000,
            body: body,
            message: message || messageConf.success.success
        }
    }, error: (error, message, status = 1001, body = false) => {
        fs.appendFile(fileName, new Date().toLocaleDateString() + ': ' + JSON.stringify(error) + "\n" || '' + "\n", function (errorR) {
            if (errorR) throw errorR;
            console.log('Saved!');
        });
        fs.appendFile(fileName, new Date().toLocaleDateString() + ': ' + error + "\n" || '' + "\n", function (errorR) {
            if (errorR) throw errorR;
            console.log('Saved!');
        });
        console.log(error, "Error");
        return {
            status: status,
            body: body,
            message: message || ""
        }
    }
};

let getDates = {
    firstLast: (from) => {
        let date = new Date(from);
        date.setDate(1);
        return {firstday: date, lastday: new Date(date.getFullYear(), date.getMonth() + 1, 1)};
    }
}
module.exports = {
    dateUtil: dateUtil,
    appUtil: appUtil,
    response: finalResponse,
    getDates: getDates
};
