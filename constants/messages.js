let success = {
    success: {
        en: "success"
    }
};
let error = {
    invalidOldPassword: {
        en: "invalid old password"
    }, internalError: {
        en: "Internal Error"
    }, duplicateEntry: {
        en: "Duplicate Entry"
    }, invalidUser: {
        en: "invalid user"
    },formAlreadyAdded: {
        en: "form already added"
    }, invalidForm: {
        en: "invalid form"
    }, userAlreadyExist: {
        en: "user already Exist"
    },
    invalidParameter: {
        en: "invalid parameters"
    },invalidData: {
        en: "invalid data"
    },
    unauthorizedUser: {
        en: "unauthorized user"
    },
    invalidToken: {
        en: "invalid token"
    }, somethingWentWrong: {
        en: "Something went wrong"
    },
    saveAgentError: {
        en: "error adding user"
    },
    duplicateUser: {
        en: "user already exist"
    },
    saveDoctorError: {
        en: "doctor adding patient"
    },
    duplicateDoctor: {
        en: "doctor already exist"
    },
    invalidVaccId: {
        en: "invalid vaccination id"
    },
    alreadyExits: {
        en: "Data already exists"
    }, invalidId: {
        en: "invalid id"
    }, invalidMember: {
        en: "invalid user name"
    }, duplicateAdmin: {
        en: "admin already exist"
    }, errorAddingAdmin: {
        en: "Admin Add failed"
    }, duplicatePharmacy: {
        en: "pharmacy already exist"
    }, errorAddingPharmacy: {
        en: "Pharmacy Add failed"
    }, duplicateProduct: {
        en: "product already exist"
    }, saveProductError: {
        en: "error adding product"
    }
};

module.exports = {
    success: success,
    error: error
};
