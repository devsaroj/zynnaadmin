let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let authentication = require('./routes/authentication');
let register = require('./routes/register');
let adminRoute = require('./routes/admin');
let agentRoute = require('./routes/Agent');
let rfs = require('rotating-file-stream');
let fs = require('fs');

/* Initiate connection*/
require("./db/mongoConnection");

let app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

let logDirectory = path.join(__dirname, 'logs');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

/* Access log with day rotator */
let accessLogStream = rfs('access.log', {
    interval: '1d', // rotate daily
    path: logDirectory
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('tiny', {stream: accessLogStream}));
// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//CORS middleware
let allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type,x-access-token');

    next();
}
app.use(allowCrossDomain);
app.all("/", (req, res) => {
    let zynna = fs.readFileSync(path.join(__dirname, 'welcome.txt'));
    res.setHeader('content-type', 'text/html');
    res.send(zynna);
});


/* Route to authenticate users */
app.use('/api/v1/authentication', authentication);
/* Route to register user */
app.use('/api/v1/register', register);
/*Route to Admin*/
app.use('/api/v1/admin', adminRoute)
/*Route to Client*/
app.use('/api/v1/client', agentRoute)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
// error handler
app.use(function (err, req, res, next) {
    let fileName = 'error.txt'
    fs.appendFile(fileName, new Date().toLocaleDateString() + ': ' + JSON.stringify(err) + "\n" || '' + "\n");
    /* If development server then show stack */
    if (req.app.get('env') === 'development') {
        res.locals.error = err;
        res.locals.message = err.message;
        console.log(err.message)
        res.status(err.status || 500);
        res.render('error');
    } else {
        /* If Production then send json response */
        res.json({
            status: 1001,
            body: {
                code: err.status || 500
            },
            message: err.message
        });
    }
});

module.exports = app;
