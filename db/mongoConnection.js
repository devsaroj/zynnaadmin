let config = require('../config/config');
let mongoose = require('mongoose');

mongoose.connect(config.db.getConnectionUrl(), {useMongoClient: true});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function () {
    console.log("Connected");
});
module.exports = db;
