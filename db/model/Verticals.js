let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Vertical = new Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        index: {
            unique: true
        }
    },
    isActive: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true,
    usePushEach: true
});

module.exports = mongoose.model('vertical', Vertical);
