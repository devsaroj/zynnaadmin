let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let citySchema = new Schema({
    name: {
        type: String,
        lowercase: true
    },
    state: {
        type: String,
        lowercase: true
    },
    country: {
        type: String,
        lowercase: true
    }
});
module.exports = mongoose.model('city', citySchema);