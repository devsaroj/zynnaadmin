let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let bcrypt = require("bcrypt");
let config = require("../../config/config");
let util = require('../../util/util');
let messageConf = require('../../constants/messages');
let CategorySchema = require("../../db/model/Category");
let VerticalSchema = require("../../db/model/Verticals");

let AgentSchema = new Schema({
        name: {type: String, required: true, lowercase: true},
        mobile: {
            number: {type: String, required: true},
            code: {type: String, required: true}
        },
        address: {type: String, required: true},
        email: {
            type: String, lowercase: true
        },
        password: {type: String, required: true},
        verticals: {
            name: {type: String, required: true},
            _id: {type: mongoose.Schema.ObjectId, required: true}
        },
        isActive: {type: Boolean, default: true},
        isRemoved: {type: Boolean, default: false},
        reset_password_token: String,
        reset_password_expires: Date
    },
    {
        timestamps: true,
        usePushEach: true
    }
    )
;

AgentSchema.pre('save', function (next) {
    let patient = this;
    // only hash the password if it has been modified (or is new)
    if (!patient.isModified('password')) return next();
    // generate a salt
    bcrypt.genSalt(config.salt, function (err, salt) {
        if (err) return next(err);
        // hash the password using our new salt
        bcrypt.hash(patient.password, salt, function (err, hash) {
            if (err) return next(err);
            // override the cleartext password with the hashed one
            patient.password = hash;
            next();
        });
    });
});

AgentSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

AgentSchema.methods.addForm = function (data, refId, next) {
    Promise.all([getCategory(), getVerticals()]).then(x => {
        const [category, vertical] = x;
        let formSchema = new FormSchema();
        formSchema.refId = refId;
        formSchema.email = data.email;
        formSchema.email1 = data.email1;
        formSchema.email2 = data.email2;
        formSchema.type = data.type;
        formSchema.title = data.title;
        formSchema.address = {
            addressLine: data.address.addressLine,
            pincode: data.address.pincode,
            city: data.address.city,
            state: data.address.state,
            country: data.address.country
        };
        formSchema.companyName = data.companyName;
        formSchema.firstName = data.firstName;
        formSchema.surname = data.surname;
        formSchema.formNumber = parseInt(data.formNumber);
        if (data.category != null && data.category != '') {
            let matchedCategory = category.find(x => (x._id) === mongoose.Types.ObjectId(data.category));
            if (matchedCategory == null) {
                return next('Invalid Category');
            } else {
                formSchema.category = {
                    _id: matchedCategory._id,
                    name: matchedCategory.name
                };
                if (data.position != null && data.position != '') {
                    let matchedPosition = category.postions.find(x => (x._id) === mongoose.Types.ObjectId(data.position));
                    if (matchedPosition == null) {
                        return next('Invalid Position');
                    } else {
                        formSchema.position = {
                            _id: matchedPosition._id,
                            name: matchedPosition.name
                        };
                    }
                } else {
                    formSchema.position = '';
                }
            }
        } else {
            formSchema.category = '';
        }

        if (data.verticals != null && data.verticals != '') {
            let matchedPosition = vertical.find(x => (x._id) === mongoose.Types.ObjectId(data.position));
            if (matchedPosition == null) {
                return next('Invalid Verticals');
            } else {
                formSchema.verticals = {
                    _id: matchedPosition._id,
                    name: matchedPosition.name
                };
            }
        } else {
            formSchema.verticals = '';
        }
        formSchema.active = true;
        formSchema.mobile = {
            number: parseInt(data.mobile.number),
            code: data.mobile.code
        };
        formSchema.alNumber = {
            number: parseInt(data.alNumber.number),
            code: data.alNumber.code
        };
        formSchema.landLine = parseInt(data.landLine);
        formSchema.website = data.website;
        formSchema.type = data.type;
        formSchema.save((err, lastFormObj) => {
            if (err) return next(err);
            return next(null, lastFormObj);
        });
    }).catch(error => {
        return next(error)
    })
}
function getCategory() {
    return new Promise((resolve, reject) => {
        CategorySchema.find({isActive: true}).populate({
            path: "postions",
            match: {
                isActive: true
            }
        }).exec((err, category) => {
            if (err) {
                reject(err);
            }
            resolve(category);
        })
    });
}

function getVerticals() {
    return new Promise((resolve, reject) => {
        VerticalSchema.find({isActive: true}).exec((err, vertical) => {
            if (err) {
                reject(err);
            }
            resolve(vertical);
        })
    });
}
module.exports = mongoose.model('Agent', AgentSchema);

