let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let bcrypt = require("bcrypt");
let config = require("../../config/config");
let FormSchema = require('../../db/model/Forms');
let AdminSchema = new Schema({
    password: {type: String, required: true},
    userName: {type: String, required: true, index: {unique: true}, lowercase: true},
    email: String,
    role: {type: String, required: true, enum: ['superadmin', 'admin']},
    isActive: Boolean
}, {
    timestamps: true,
    usePushEach: true
});

AdminSchema.pre('save', function (next) {
    let admin = this;
    // only hash the password if it has been modified (or is new)
    if (!admin.isModified('password')) return next();
    // generate a salt
    bcrypt.genSalt(config.salt, function (err, salt) {
        if (err) return next(err);
        // hash the password using our new salt
        bcrypt.hash(admin.password, salt, function (err, hash) {
            if (err) return next(err);
            // override the cleartext password with the hashed one
            admin.password = hash;
            next();
        });
    });
});

AdminSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

AdminSchema.methods.addForm = function (data, refId, next) {
    let formSchema = new FormSchema();
    formSchema.refId = refId;
    formSchema.email = data.email;
    formSchema.email1 = data.email1;
    formSchema.email2 = data.email2;
    formSchema.type = data.type;
    formSchema.title = data.title;
    formSchema.address = {
        addressLine: data.address.addressLine,
        pincode: data.address.pincode,
        city: data.address.city,
        state: data.address.state,
        country: data.address.country
    };
    formSchema.companyName = data.companyName;
    formSchema.firstName = data.firstName;
    formSchema.surname = data.surname;
    formSchema.formNumber = parseInt(data.formNumber);
    formSchema.category = {
        _id: data.category._id,
        name: data.category.name
    };
    if (typeof data.position != "undefined" && data.position != null && data.position != '') {
        formSchema.position = {
            _id: data.position._id,
            name: data.position.name
        }
    }
    if (typeof data.verticals != "undefined" && data.verticals != null && data.verticals != '') {
        formSchema.verticals = {
            _id: data.verticals._id,
            name: data.verticals.name
        };
    }

    formSchema.active = true;
    formSchema.mobile = {
        number: parseInt(data.mobile.number),
        code: data.mobile.code
    };
    formSchema.alNumber = {
        number: parseInt(data.alNumber.number),
        code: data.alNumber.code
    };
    formSchema.landLine = parseInt(data.landLine);
    formSchema.website = data.website;
    formSchema.type = data.type;
    formSchema.save((err, lastFormObj) => {
        if (err) return next(err);
        return next(null, lastFormObj);
    });
}

module.exports = mongoose.model('Admin', AdminSchema);