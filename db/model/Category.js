let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let FormSchema = require('./Forms');

let Positions = new Schema({
    name: {
        type: String,
        required: true
    },
    isActive: Boolean
}, {
    timestamps: true,
    usePushEach: true
});

let Category = new Schema({
    name: {
        type: String,
        lowercase: true,
        required: true,
        index: {
            unique: true
        }
    },
    isActive: Boolean,
    postions: [Positions]
}, {
    timestamps: true,
    usePushEach: true
});


Category.post('save', function () {
    let cat = this;
    FormSchema.updateMany({$and:[
        {"category._id": cat._id},
        {"category.name":{$ne:cat.name}}
    ]}, {
        $set: {
            "category.name": cat.name
        }
    }).exec();
});

module.exports = mongoose.model('category', Category);

