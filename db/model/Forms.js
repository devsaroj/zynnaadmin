let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let bcrypt = require("bcrypt");
let config = require("../../config/config");
let util = require('../../util/util');
let messageConf = require('../../constants/messages');

let FormSchema = new Schema(
    {
        refId: {type: mongoose.Schema.ObjectId},
        formNumber: {type: String, required: true},
        companyName: {type: String, lowercase: true},
        title: {type: String, lowercase: true},
        surname: {type: String, lowercase: true},
        firstName: {type: String, lowercase: true},
        category: {
            name: {type: String},
            _id: {type: mongoose.Schema.ObjectId}
        },
        position: {
            name: {type: String},
            _id: {type: mongoose.Schema.ObjectId}
        },
        verticals: {
            name: {type: String, required: true},
            _id: {type: mongoose.Schema.ObjectId, required: true}
        },
        address: {
            addressLine: {type: String, lowercase: true},
            city: {type: String, lowercase: true},
            state: {type: String, lowercase: true},
            country: {type: String, lowercase: true},
            pincode: {type: String, lowercase: true},
        },
        mobile: {
            number: {type: String},
            code: String
        },
        alNumber: {
            number: {type: String},
            code: String
        },
        landLine: {type: String},
        website: {type: String, lowercase: true},
        type: {type: String, lowercase: true},
        email: {type: String},
        email1: {type: String},
        email2: {type: String},
        active: Boolean
    }, {
        timestamps: true,
        usePushEach: true
    }
);

FormSchema.methods.removeForm = function (ids, next) {
    let agent = this
    ids.map((id) => {
        let form = agent.forms.id(id);
        if (util.appUtil.isNull(form)) return next(messageConf.error.invalidId);
        form.remove();
    });
    agent.save((err) => {
        if (err) return next(err);
        return next();
    })
};

FormSchema.methods.addForm = function (data, next) {
    let formSchema = new FormSchema();
    formSchema.email = data.email;
    formSchema.email1 = data.email1;
    formSchema.email2 = data.email2;
    formSchema.type = data.type;
    formSchema.title = data.title;
    formSchema.address = {
        addressLine: data.address.addressLine,
        pincode: data.address.pincode,
        city: data.address.city,
        state: data.address.state,
        country: data.address.country
    };
    formSchema.companyName = data.companyName;
    formSchema.firstName = data.firstName;
    formSchema.surname = data.surname;
    formSchema.formNumber = parseInt(data.formNumber);
    formSchema.category = {
        _id: data.category._id,
        name: data.category.name
    };
    formSchema.position = {
        _id: data.position._id,
        name: data.position.name
    }
    formSchema.verticals = {
        _id: data.verticals._id,
        name: data.verticals.name
    };
    formSchema.active = true;
    formSchema.mobile = {
        number: parseInt(data.mobile.number),
        code: data.mobile.code
    };
    formSchema.alNumber = {
        number: parseInt(data.alNumber.number),
        code: data.alNumber.code
    };
    formSchema.landLine = parseInt(data.landLine);
    formSchema.website = data.website;
    formSchema.type = data.type;
    formSchema.save((err, lastFormObj) => {
        if (err) return next(err);
        return next(null, lastFormObj.forms[lastFormObj.forms.length - 1]);
    });
};

FormSchema.methods.updateForm = function (data, next) {
    let formSchema = this
    formSchema.email = data.email;
    formSchema.email1 = data.email1;
    formSchema.email2 = data.email2;
    formSchema.type = data.type;
    formSchema.title = data.title;
    formSchema.address = {
        addressLine: data.address.addressLine,
        pincode: data.address.pincode,
        city: data.address.city,
        state: data.address.state,
        country: data.address.country
    };
    formSchema.companyName = data.companyName;
    formSchema.firstName = data.firstName;
    formSchema.surname = data.surname;
    if (typeof data.category != "undefined" && data.category != null && data.category != '') {
        formSchema.category = {
            _id: data.category._id,
            name: data.category.name
        };
    }
    if (typeof data.position != "undefined" && data.position != null && data.position != '') {
        formSchema.position = {
            _id: data.position._id,
            name: data.position.name
        }
    }
    if (typeof data.verticals != "undefined" && data.verticals != null && data.verticals != '') {
        formSchema.verticals = {
            _id: data.verticals._id,
            name: data.verticals.name
        };
    }

    formSchema.active = true;
    formSchema.mobile = {
        number: parseInt(data.mobile.number),
        code: data.mobile.code
    };
    formSchema.alNumber = {
        number: parseInt(data.alNumber.number),
        code: data.alNumber.code
    };
    formSchema.landLine = parseInt(data.landLine);
    formSchema.website = data.website;
    formSchema.type = data.type;
    formSchema.save((err, lastFormObj) => {
        if (err) return next(err);
        return next(null, lastFormObj);
    });
}
module.exports = mongoose.model('Forms', FormSchema);

