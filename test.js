let Excel = require('exceljs');
require('./db/mongoConnection');
let CategorySchema = require("./db/model/Category");
let VerticalSchema = require("./db/model/Verticals");

setTimeout(function () {
    Promise.all([getCategory(), getVerticals()]).then(x => {
        const [category, vertical] = x;

        let invalidRows = [];
        let successCount = 0;

        let workbook = new Excel.Workbook();
        workbook.xlsx.readFile("TEMPLATE.xlsx")
            .then(function () {
                workbook.eachSheet(function (worksheet) {
                    let header = null;
                    worksheet.eachRow(function (row) {
                        if (header == null) {
                            header = row.values;
                        } else {
                            let newRow = {};
                            for (let index = 0; index < row.values.length; index++) {
                                newRow[header[index]] = row.values[index];
                            }
                            let matchedCategory = category.find(x => (x.name || "").toLowerCase() === newRow['CATEGORY'].toLowerCase());
                            if (matchedCategory == null) {
                                newRow['error'] = "Invalid Category";
                                invalidRows.push(newRow);
                            } else {
                                let matchedPosition = matchedCategory.postions.find(x => (x.name || "").toLowerCase() === newRow['POSITION'].toLowerCase())
                                if (matchedPosition == null) {
                                    newRow['error'] = "Invalid Position";
                                    invalidRows.push(newRow);
                                } else {
                                    let matchedVertical = vertical.find(x => (x.name || "").toLowerCase() === newRow['VERTICAL'].toLowerCase())
                                    if (matchedVertical == null) {
                                        newRow['error'] = "Invalid Vertical";
                                        invalidRows.push(newRow);
                                    } else {
                                        /* here add data to data base  */
                                        successCount++;
                                    }
                                }
                            }
                        }

                    });
                    console.log(`invalid rows: ${JSON.stringify(invalidRows)}, Success Count: ${successCount}`);
                });
            });


    }).catch(error => {

    });
}, 500);

function getCategory() {
    return new Promise((resolve, reject) => {
        CategorySchema.find({isActive: true}).populate({
            path: "postions",
            match: {
                isActive: true
            }
        }).exec((err, category) => {
            if (err) {
                reject(err);
            }
            resolve(category);
        })
    });
}

function getVerticals() {
    return new Promise((resolve, reject) => {
        VerticalSchema.find({isActive: true}).exec((err, vertical) => {
            if (err) {
                reject(err);
            }
            resolve(vertical);
        })
    });
}

/*
workbook.creator = 'Me';
workbook.lastModifiedBy = 'Her';
workbook.created = new Date(1985, 8, 30);
workbook.modified = new Date();
workbook.lastPrinted = new Date(2016, 9, 27);
workbook.views = [
    {
        x: 0, y: 0, width: 10000, height: 20000,
        firstSheet: 0, activeTab: 1, visibility: 'visible'
    }
];
let worksheet = workbook.addWorksheet('My Sheet');

worksheet.columns = [
    {header: 'Id', key: 'id', width: 10},
    {header: 'Name', key: 'name', width: 32},
    {header: 'D.O.B.', key: 'DOB', width: 10, outlineLevel: 1}
];

worksheet.addRow({id: 1, name: 'John Doe', DOB: new Date(1970, 1, 1)});
worksheet.addRow({id: 2, name: 'Jane Doe', DOB: new Date(1965, 1, 7)});

workbook.xlsx.writeFile('newfile.xlsx').then(function (x) {
    console.log(x);
});*/
