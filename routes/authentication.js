let express = require('express');
let router = express.Router();
let util = require('../util/util');
let adminCtrl = require('../controller/adminCtrl');
let agentCtrl = require('../controller/agentCtrl');
let agentSchema = require('../db/model/Agent');
let message = require('../constants/messages');
let bcrypt = require('bcrypt');

router.post('/admin', function (req, res) {
    const {userName, password} = req.body;
    if (util.appUtil.empty(userName) || util.appUtil.empty(password)) {
        let error = message.error.invalidParameter;
        return res.json(util.response.error(error, error));
    }

    adminCtrl.authenticate(userName, password).then((admin) => {
        /* Authenticated User */
        let encoded = util.appUtil.signToken(admin);
        return res.json(util.response.success({token: encoded, userData: admin}));
    }).catch((error) => {
        return res.json(util.response.error(error, message.error.unauthorizedUser));
    })
});

router.post('/agent', function (req, res) {
    const {userName, password} = req.body;
    if (util.appUtil.empty(userName) || util.appUtil.empty(password)) {
        let error = message.error.invalidParameter;
        return res.json(util.response.error(error, error));
    }

    agentCtrl.authenticate(userName, password).then((patient) => {
        /* Authenticated User */
        let encoded = util.appUtil.signToken(patient);
        return res.json(util.response.success({token: encoded}));
    }).catch((error) => {
        return res.json(util.response.error(error, message.error.unauthorizedUser));
    });
});

router.post('/reset/password/initialize', (req, res) => {
    const {userName} = req.body;
    if (util.appUtil.empty(userName)) {
        let error = message.error.invalidParameter;
        return res.json(util.response.error(error, error));
    }
    agentSchema.findOne({'email': userName}).exec((error, person) => {
        if (error || !person) return res.json(util.response.error(error, message.error.invalidUser));
        util.appUtil.generateRandomToken().then((token) => {
            agentSchema.findByIdAndUpdate(person._id,
                {
                    $set: {
                        reset_password_token: token,
                        reset_password_expires: Date.now() + 86400000
                    }
                }, {new: true}).exec((err, new_person) => {
                console.log(new_person)
                let hostname = req.protocol + '://' + req.get('host');
                let resetUrl = `${hostname}/api/v1/authentication/reset/password/redirect/${token}`;
                let mailOptions = {
                    from: 'internal@zynna.in',
                    to: new_person.email,
                    subject: 'Zynna | password reset',
                    html: `<p><h3>Dear ${person.name},</h3>
                <p>You requested for a password reset, kindly use this 
                <a  href="${resetUrl}">link</a> to reset your password</p>
                </p>`
                };
                util.appUtil.sendMail(mailOptions,).then(() => {
                }).catch((error) => {
                    console.log('Email not sent')
                });
                return res.json(util.response.success());
            });
        });
    });
});

router.get('/reset/password/redirect/:token', (req, res) => {
    const {token} = req.params;
    console.log(token)
    res.render('reset_password', {
        topicHead: 'Password Reset',
        token: token
    });
});
router.post('/reset/password/form', (req, res) => {
    const {newp, cnew, token, submit} = req.body;
    console.log(token)
    agentSchema.findOne({
        reset_password_token: token,
        reset_password_expires: {
            $gt: Date.now()
        }
    }).exec((err, user) => {
        console.log(user, 'sda')
        if (!err && user) {
            if (newp === cnew) {
                user.hash_password = bcrypt.hashSync(newp, 10);
                user.reset_password_token = undefined;
                user.reset_password_expires = undefined;
                user.save(function (err) {
                    if (err) {
                        return res.status(422).send({
                            message: err
                        });
                    } else {
                        let hostname = req.headers.host;
                        let mailOptions = {
                            from: 'internal@zynna.in',
                            to: user.email,
                            subject: 'Password Reset confirmation',
                            html: `<p>password reset successfully</p>`
                        };
                        util.appUtil.sendMail(mailOptions,).then(() => {
                            return res.status(200).send({
                                message: 'password reset successfully'
                            });
                        }).catch((error) => {
                            console.log('Email not sent')
                        });
                    }
                });
            } else {
                return res.status(422).send({
                    message: 'Passwords do not match'
                });
            }
        } else {
            return res.status(400).send({
                message: 'Password reset token is invalid or has expired.'
            });
        }
    });
});
module.exports = router;
