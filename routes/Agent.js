let express = require('express');
let router = express.Router();
let util = require('../util/util');
let AjentSchema = require('../db/model/Agent');
let FormSchema = require('../db/model/Forms');
let messageConf = require('../constants/messages');
let path = require('path');
let agentCtrl = require('../controller/agentCtrl');
let mongoose = require('mongoose');

router.use(function (req, res, next) {
    if (req.method === 'OPTIONS')
        return next();
    let token = req.headers["x-access-token"];
    if (typeof token === "undefined") {
        return res.json(util.response.error(messageConf.error.unauthorizedUser, messageConf.error.unauthorizedUser, 5000));
    } else {
        util.appUtil.verifyToken(token).then((decoded) => {
            req.decoded = decoded;
            next();
        }).catch((error) => {
            return res.json(util.response.error(messageConf.error.invalidToken, messageConf.error.invalidToken, 5001));
        });
    }
});
router.post('/addForm', (req, res) => {
    let token = req.decoded;
    let agentId = token._id;
    const {
        formNumber, companyName, title,
        surname, firstName, category, email, email1, email2, position, verticals, address, mobile, alNumber, landLine,
        website, type, isValid
    } = req.body;
    if (util.appUtil.empty(formNumber) || util.appUtil.empty(verticals)
    ) {
        return res.send(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter, 1002));
    }
    AjentSchema.findOne({_id: mongoose.Types.ObjectId(agentId), isActive: true, isRemoved: false}, (err, agent) => {
        if (err) {
            /* Error fetching agent */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!agent) {
            /* Invalid agent */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        FormSchema.findOne({refId: mongoose.Types.ObjectId(agentId), formNumber: formNumber}, (err, uniqueForm) => {
            if (err) {
                /* Error fetching agent */
                return res.json(util.response.error(err, messageConf.error.internalError))
            }
            if (uniqueForm) {
                /* Dulplicate Form found */
                return res.json(util.response.error(messageConf.error.formAlreadyAdded, messageConf.error.formAlreadyAdded));
            }
        });
        if (!isValid) {
            let searchFields = [];
            let emailField = {}
            let mobileField = {};
            let companyField = {};
            if (email == '' && mobile.number == '' && companyName == '') {
                return res.json(util.response.success([]));
            }

            if (email != null && email != '') {
                emailField.email = new RegExp('^' + email.toLowerCase().trim() + '$', 'i');
                searchFields.push(emailField)
            }
            if (mobile.number != null && mobile.number != '') {

                mobileField['mobile.number'] = new RegExp('^' + mobile.number.toLowerCase().trim() + '$', 'i');
                searchFields.push(mobileField);
            }
            if (companyName != null && companyName != '') {
                companyField.companyName = new RegExp('^' + companyName.toLowerCase().trim() + '$', 'i');
                searchFields.push(companyField);
            }
            let $match = {
                "refId": mongoose.Types.ObjectId(agentId),
                "active": true
            }

            if (emailField.email != null || mobileField['mobile.number'] != null || companyField.companyName != null) {
                $match['$or'] = searchFields
            }
            // console.log(searchFields, 'searchFields')
            // console.log($match, 'match')
            FormSchema.aggregate([
                {
                    $match: $match
                },
            ]).exec(function (err, forms) {
                if (err) return res.json(util.response.error(err, messageConf.error.internalError))
                let results = [];
                if (forms != null) {
                    results = forms;
                }
                return res.json(util.response.success(results));

            });
        } else {
            agentCtrl.addForm(req.body, agentId).then((form) => {
                /* Authenticated User */
                return res.json(util.response.success(form));
            }).catch((error) => {
                return res.json(util.response.error(error, error));
            });
        }
    });
});
router.get('/getSubmittedForms', (req, res) => {
    let token = req.decoded;
    let agentId = token._id;
    AjentSchema.findOne({_id: mongoose.Types.ObjectId(agentId), isActive: true, isRemoved: false}, (err, agent) => {
        if (err) {
            /* Error fetching agent */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!agent) {
            /* Invalid agent */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        FormSchema.aggregate([
            {
                $match: {
                    "refId": mongoose.Types.ObjectId(agentId),
                    "active": true
                }
            },
        ]).exec(function (err, forms) {
            if (err) return res.json(util.response.error(err, messageConf.error.internalError))
            let results = [];
            if (forms != null) {
                results = forms;
            }
            return res.json(util.response.success(results));
        });
    });
});
module.exports = router;