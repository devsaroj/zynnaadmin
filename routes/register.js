let express = require('express');
let router = express.Router();
let util = require('../util/util');
let AdminSchema = require('../db/model/Admin');
let AgentSchema = require('../db/model/Agent');
let messageConf = require('../constants/messages');

router.post('/admin', function (req, res, next) {
    const {
        userName, password, role, email, isActive
    } = req.body;
    if (util.appUtil.empty(userName) || util.appUtil.empty(password) || util.appUtil.empty(role)) {
        return res.send(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter, 1002));
    }

    let admin = new AdminSchema();
    admin.userName = userName;
    admin.email = email;
    admin.role = role;
    admin.password = password;
    admin.isActive = isActive;

    admin.save(function (err) {
        if (err) {
            console.log(err)
            if (err.code === 11000)
                return res.json(util.response.error(err, messageConf.error.duplicateAdmin));
            return res.json(util.response.error(err, messageConf.error.errorAddingAdmin));
        }
        return res.json(util.response.success());
    });
});

router.post('/agent', function (req, res, next) {
    const {
        name, mobile,
        address, email, isActive, verticals, isRemoved
    } = req.body;
    if (util.appUtil.empty(email) || util.appUtil.empty(verticals)) {
        return res.send(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter, 1002));
    }
    let password = '1234';
    let agent = new AgentSchema();
    agent.name = name;
    agent.mobile = {
        number: parseInt(mobile.number),
        code: mobile.code
    }
    agent.verticals = {
        name: verticals.name,
        _id: verticals._id
    }
    agent.address = address;
    agent.email = email;
    agent.password = password;
    agent.isActive = isActive;
    agent.isRemoved = isRemoved;
    AgentSchema.find({email: email, isRemoved: false}).exec((err, ver) => {
        if (err)
            return res.json(util.response.error(err, messageConf.error.error));
        if (ver.length === 0) {
            /*valid user*/
            agent.save(function (err) {
                if (err) {
                    if (err.toJSON().code === 11000)
                        return res.json(util.response.error(err, messageConf.error.duplicateUser));
                    return res.json(util.response.error(err, messageConf.error.saveAgentError));
                }
                let mailOptions = {
                    from: 'internal@zynna.in',
                    to: email,
                    subject: 'Welcome to Zynna!',
                    html: `<p>User Name: ${email} <br>Password : ${password}</p>`
                };
                util.appUtil.sendMail(mailOptions).then(() => {
                }).catch((error) => {
                    console.log(error)
                    console.log('Email not sent')
                });
                return res.json(util.response.success());
            });
        } else {
            return res.json(util.response.error(messageConf.error.userAlreadyExist, messageConf.error.userAlreadyExist));
        }
    });
});

module.exports = router;