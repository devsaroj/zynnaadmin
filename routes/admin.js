let express = require('express');
let router = express.Router();
let util = require('../util/util');
let AdminSchema = require('../db/model/Admin');
let AgentSchema = require('../db/model/Agent');
let FormsSchema = require('../db/model/Forms');
let CitySchema = require('../db/model/City');
let VerticalSchema = require('../db/model/Verticals');
let CatSchema = require('../db/model/Category');
let adminCtrl = require('../controller/adminCtrl');
let categoryCtrl = require('../controller/categoryCtrl');
let messageConf = require('../constants/messages');
let path = require('path');
let mongoose = require('mongoose');
let fs = require('fs');
let fs_extra = require('fs-extra');
var multer = require('multer');
let uploadDirectory = path.join('uploads');
fs.existsSync(uploadDirectory) || fs.mkdirSync(uploadDirectory);
let upload = multer({dest: uploadDirectory});
let nodemailer = require('nodemailer');

router.post('/forgot-password', (req, res) => {
    const {hint} = req.body;
    let authUser = "internal@zynna.in";
    let authPass = "asdf@856$#";
    AdminSchema.findOne({
        userName: hint.trim().toString().toLowerCase() || ''
    }).exec((err, user) => {
        if (err)
            return res.json(util.response.error(err, messageConf.error.somethingWentWrong));
        if (!user)
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));

        let email = user.email;
        let password = util.appUtil.generatePassword();
        let author = "Zynna";
        user.password = password;
        user.save(err => {
            if (err)
                return res.json(util.response.error(err, messageConf.error.somethingWentWrong));

            let transporter = nodemailer.createTransport({
                host: 'mail.zynna.in',
                port: 587,
                secure: false,
                tls: {
                    rejectUnauthorized: false
                },
                auth: {
                    user: authUser,
                    pass: authPass
                }
            });
            let html = `<html>
                        <head></head>
                        <body>
                        <p>Hi&nbsp;</p>
                        <p>Seems like you forgot your password, we have auto generated a new password for you.</p>
                        <p>Note* you will not me able login with your old password</p>
                        <p>Your new password is&nbsp;<strong>${password}</strong></p>
                        <p>Regards<br><span>${author}</span></p>
                        </body>
                        </html>`;
            let mailOptions = {
                from: authUser,
                to: email, // list of receivers
                subject: 'Zynna Admin | Forgot Password', // Subject line
                text: 'You password is ' + password, // plain text body
                html: html // html body
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error)
                    return res.json(util.response.error(error, messageConf.error.somethingWentWrong));
                }
                return res.json(util.response.success());
            });
        });
    });
});
router.get('/download/:fileName', (req, res) => {
    const {fileName} = req.params;
    let filePth = path.join(__dirname, '../', fileName);
    let stream = fs.createReadStream(filePth);
    stream.on('close', () => {
        fs.unlink(filePth);
    });
    stream.pipe(res);
});
router.get('/downloadTemplate/:fileName', (req, res) => {
    const {fileName} = req.params;
    let filePth = path.join(__dirname, '../', fileName);
    let stream = fs.createReadStream(filePth);
    stream.pipe(res);
});
router.get('/downloadFailedTemplate/:fileName', (req, res) => {
    const {fileName} = req.params;
    let filePth = path.join(__dirname, '../', 'public', 'uploads', fileName);
    let stream = fs.createReadStream(filePth);
    stream.pipe(res);
});
router.get('/downloadFile/:type/:formId', (req, res) => {
    const {type, formId} = req.params;
    FormsSchema.findById(mongoose.Types.ObjectId(formId), (err, form) => {
        if (err) {
            /* Error fetching form */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!form) {
            /* Invalid form */
            return res.json(util.response.error(messageConf.error.invalidForm, messageConf.error.invalidForm));
        }
        adminCtrl.createFile([form], type).then((fileName) => {
            let filePth = path.join(__dirname, '../', fileName);
            return res.json(util.response.success(fileName));
        }).catch((error) => {
            return res.json(util.response.error(error, error));
        });

    });

});
router.post('/downloadFileMulti', (req, res) => {
    const {type, formIds} = req.body;
    if (!Array.isArray(formIds)) {
        const skip = (formIds.pageNo - 1) * formIds.limit;
        adminCtrl.getAllSubmittedForms(formIds.limit, formIds.pageNo, formIds.search, skip, formIds.from, formIds.to).then((forms) => {
            adminCtrl.createFile(forms, type).then((fileName) => {
                let filePth = path.join(__dirname, '../', fileName);
                return res.json(util.response.success(fileName));
            }).catch((error) => {
                return res.json(util.response.error(error, error));
            });
        }).catch((error) => {
            return res.json(util.response.error(error, error));
        });
    } else {
        FormsSchema.find({
            '_id': {$in: formIds}
        }, function (err, forms) {
            if (err) {
                /* Error fetching form */
                return res.json(util.response.error(err, messageConf.error.internalError))
            }
            adminCtrl.createFile(forms, type).then((fileName) => {
                let filePth = path.join(__dirname, '../', fileName);
                return res.json(util.response.success(fileName));
            }).catch((error) => {
                return res.json(util.response.error(error, error));
            });
        });
    }
});
router.use(function (req, res, next) {
    if (req.method === 'OPTIONS')
        return next();
    let token = req.headers["x-access-token"];
    if (typeof token === "undefined") {
        return res.json(util.response.error(messageConf.error.unauthorizedUser, messageConf.error.unauthorizedUser, 5000));
    } else {
        util.appUtil.verifyToken(token).then((decoded) => {
            req.decoded = decoded;
            next();
        }).catch((error) => {
            return res.json(util.response.error(messageConf.error.invalidToken, messageConf.error.invalidToken, 5001));
        });
    }
});
router.post('/bulkUpload/forms', upload.single('xlsxfile'), (req, res) => {
    let token = req.decoded;
    let adminId = token._id;
    let file = req.file;
    AdminSchema.findById(adminId, (err, admin) => {
        if (err) {
            /* Error fetching admin */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!admin) {
            /* Invalid admin */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        let uploadDir = path.join(__dirname, '../', 'public', 'uploads');
        fs_extra.ensureDirSync(uploadDir) || fs_extra.mkdirsSync(uploadDir);
        let tempArray = file.originalname.split('.');
        let ext = tempArray[tempArray.length - 1];
        let fileName = Math.ceil(Math.random() * 10000).toString() + new Date().getTime() + '.' + ext;
        let invalidfileName = Math.ceil(Math.random() * 10000).toString() + new Date().getTime() + '.' + ext;

        fs_extra.move(file.path, `${uploadDir}/${fileName}`, err => {
            if (err) return res.json(util.response.error(err, messageConf.error.somethingWentWrong));

            let fileFullPath = `${uploadDir}/${fileName}`;
            let invalidfileFullPath = `${uploadDir}/`;
            adminCtrl.bulkUploadForm(fileFullPath).then((validRows) => {
                adminCtrl.addForm(validRows, adminId).then((ress) => {
                    fs.unlink(fileFullPath);
                    return res.json(util.response.success(true, `Added ${ress.length} row(s)`))
                }).catch((err) => {
                    return res.json(util.response.error(err, messageConf.error.somethingWentWrong))
                });
            }).catch((errorData) => {
                adminCtrl.addForm(errorData.validRows, adminId).then((ress) => {
                    fs.unlink(fileFullPath);
                    adminCtrl.createInvalidFormsFile(errorData.invalidRows, invalidfileFullPath).then((invalidFileName) => {
                        return res.json(util.response.success(invalidFileName, `Added ${ress.length} row(s) and failed to add ${errorData.invalidRows.length}`));
                    }).catch((err) => {
                        console.log(err)
                        return res.json(util.response.error(err, messageConf.error.somethingWentWrong))
                    });
                }).catch((err) => {
                    return res.json(util.response.error(err, messageConf.error.somethingWentWrong))
                });
            });
        });

    });
});
router.post('/addForm', (req, res) => {
    let token = req.decoded;
    let agentId = token._id;
    const {
        formNumber, companyName, title,
        surname, firstName, category, email, email1, email2, position, verticals, address, mobile, alNumber, landLine,
        website, type, isValid
    } = req.body;
    if (util.appUtil.empty(formNumber)
        || util.appUtil.empty(verticals)) {
        return res.send(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter, 1002));
    }
    AdminSchema.findById(agentId, (err, admin) => {
        if (err) {
            /* Error fetching patient */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!admin) {
            /* Invalid patient */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        admin.addForm(req.body, agentId, (err, lastInsertFormObj) => {
            if (err) return res.json(util.response.error(messageConf.error.internalError, messageConf.error.internalError));
            return res.json(util.response.success());
        });
    });
});
router.get('/getUsersSubmittedForms', (req, res) => {
    let token = req.decoded;
    let agentId = token._id;
    AdminSchema.findById(agentId, (err, admin) => {
        if (err) {
            /* Error fetching patient */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!admin) {
            /* Invalid patient */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        FormsSchema.aggregate([
            {
                $group: {_id: '$refId'}
            }, {
                $lookup: {
                    from: 'admins',
                    let: {"refId": "$_id"},
                    pipeline: [{
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$refId"]
                            }
                        }
                    }, {
                        $project: {
                            name: "$userName"
                        }
                    }],
                    as: 'adminJoins'
                }
            }, {
                $lookup: {
                    from: 'agents',
                    let: {"refId": "$refId"},
                    pipeline: [{
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$refId"]
                            }
                        }
                    }, {
                        $project: {
                            name: 1
                        }
                    }],
                    as: 'agentsJoins'
                }
            }
        ]).exec(function (err, forms) {
            if (err) return res.json(util.response.error(messageConf.error.internalError, messageConf.error.internalError));
            let results = [];
            if (forms != null) {
                results = forms;
            }
            return res.json(util.response.success(results));
        });
    });
});
router.post('/upDateForm', (req, res) => {
    let token = req.decoded;
    let agentId = token._id;
    const {
        formNumber, companyName, title,
        surname, firstName, category, email, email1, email2, position, verticals, address, mobile, alNumber, landLine,
        website, type, isValid,
        formId
    } = req.body;
    if (util.appUtil.empty(formNumber)
        || util.appUtil.empty(verticals)) {
        return res.send(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter, 1002));
    }
    AdminSchema.findById(agentId, (err, admin) => {
        if (err) {
            /* Error fetching patient */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!admin) {
            /* Invalid patient */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        FormsSchema.findById(formId, (err, form) => {
            if (err) {
                /* Error fetching form */
                return res.json(util.response.error(err, messageConf.error.internalError))
            }
            if (!form) {
                /* Invalid form */
                return res.json(util.response.error(messageConf.error.invalidForm, messageConf.error.invalidForm));
            }
            form.updateForm(req.body, (err) => {
                if (err)
                    return res.json(util.response.error(err, err));
                return res.json(util.response.success());
            })
        })
    });
});
router.post('/getSubmittedForms', (req, res) => {
    let token = req.decoded;
    let AdminId = token._id;
    const {limit, pageNo, search, from, to, filter} = req.body;
    const skip = (pageNo - 1) * limit;
    AdminSchema.findById(AdminId, (err, admin) => {
        if (err) {
            /* Error fetching patient */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!admin) {
            /* Invalid Admin */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        adminCtrl.getSubmittedForms(limit, pageNo, search, skip, from, to,filter).then((forms) => {
            return res.json(util.response.success(forms));
        }).catch((error) => {
            return res.json(util.response.error(error, error));
        });
    });
});
router.get('/dashboard/top/getSubmittedForms', (req, res) => {
    let token = req.decoded;
    let AdminId = token._id;
    AdminSchema.findById(AdminId, (err, admin) => {
        if (err) {
            /* Error fetching patient */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!admin) {
            /* Invalid Admin */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
        adminCtrl.getDashboardCardsData().then((dashboardCards) => {
            return res.json(util.response.success(dashboardCards));
        }).catch((error) => {
            return res.json(util.response.error(error, error));
        });
    });
});
router.post('/removeForm', (req, res) => {
    let token = req.decoded;
    let AdminId = token._id;
    const {ids} = req.body;
    if (util.appUtil.empty(ids)) {
        let error = messageConf.error.invalidParameter;
        return res.json(util.response.error(error, error));
    }

    FormsSchema.updateMany(
        {_id: {$in: ids}},
        {$set: {active: false}}, function (err) {
            if (err) return res.json(util.response.error(err, messageConf.error.internalError));
            return res.json(util.response.success());
        }
    );
});
router.post('/add/category', (req, res) => {
    const {name, isActive} = req.body;

    if (util.appUtil.empty(name) || util.appUtil.empty(isActive)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }

    let category = new CatSchema();
    category.name = name;
    category.isActive = isActive;

    category.save(error => {
        if (error) {
            if (error.toJSON().code === 11000)
                return res.json(util.response.error(error, messageConf.error.duplicateEntry));
            return res.json(util.response.error(error, messageConf.error.error));
        } else {
            return res.json(util.response.success());
        }
    });
});
router.post('/get/category', (req, res) => {
    const {updateDate, isActive} = req.body;
    categoryCtrl.getLatestCategory(updateDate, isActive, (err, category) => {
        if (err)
            return res.json(util.response.error(err, messageConf.error.error));
        return res.json(util.response.success(category))
    });
});
router.get('/get/category/forfilters', (req, res) => {
    adminCtrl.getFilterCategoriesAndVerticals().then((filterData) => {
        return res.json(util.response.success(filterData));
    }).catch((error) => {
        return res.json(util.response.error(error, error));
    });
});
router.get('/get/category/:id', (req, res) => {
    let id = req.params.id;
    if (util.appUtil.empty(id) || !mongoose.Types.ObjectId.isValid(id)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }
    CatSchema.findById(id).exec((err, category) => {
        if (err)
            return res.json(util.response.error(err, messageConf.error.error));
        return res.json(util.response.success(category));
    });
});
router.post('/add/position', (req, res) => {
    const {id, name, isActive} = req.body;
    if (util.appUtil.empty(id) || util.appUtil.empty(name) || util.appUtil.empty(isActive)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }

    CatSchema.findById(id).exec((error, cat) => {
        if (error)
            return res.json(util.response.error(error, messageConf.error.error));
        let subcat = {
            name: name,
            isActive: isActive
        };
        if (cat.postions.find(x => x.name.toLowerCase() === name.toLowerCase()) &&
            cat.postions.find(x => x.isActive === true
            )) {
            return res.json(util.response.error(messageConf.error.duplicateEntry, messageConf.error.duplicateEntry));
        }
        cat.postions.push(subcat);
        cat.save(error => {
            if (error) {
                return res.json(util.response.error(error, messageConf.error.error));
            } else {
                return res.json(util.response.success());
            }
        });

    })
});
router.post('/add/vertical', (req, res) => {
    const {verticalId, name, isActive} = req.body;
    if (util.appUtil.empty(name)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }
    if (util.appUtil.empty(verticalId)) {
        let vertical = new VerticalSchema();
        vertical.name = name;
        vertical.isActive = isActive;
        vertical.save((err) => {
            if (err) {
                if (err.toJSON().code === 11000)
                    return res.json(util.response.error(err, messageConf.error.duplicateEntry));
                return res.json(util.response.error(err, messageConf.error.error));
            } else {
                return res.json(util.response.success());
            }
        })
    } else {
        if (!util.appUtil.empty(verticalId)) {
            VerticalSchema.findById(verticalId).exec((err, vertical) => {
                if (err)
                    return res.json(util.response.error(err, messageConf.error.error));
                if (!vertical) {
                    return res.json(util.response.error(messageConf.error.invalidData, messageConf.error.invalidData));
                }
                vertical.name = name;
                vertical.isActive = isActive;
                vertical.save((err) => {
                    if (err) {
                        if (err.toJSON().code === 11000)
                            return res.json(util.response.error(err, messageConf.error.duplicateEntry));
                        return res.json(util.response.error(err, messageConf.error.error));
                    } else {
                        return res.json(util.response.success());
                    }
                })
            })
        }
    }
});

router.post('/get/vertical', (req, res) => {
    const {updateDate, isActive} = req.body;
    categoryCtrl.getLatestVertical(updateDate, isActive, (err, category) => {
        if (err)
            return res.json(util.response.error(err, messageConf.error.error));
        return res.json(util.response.success(category))
    })
});
router.get('/get/users', (req, res) => {
    AgentSchema.find({'isRemoved': false}).exec((err, users) => {
        if (err)
            return res.json(util.response.error(err, messageConf.error.error));
        return res.json(util.response.success(users))
    });
});
router.post('/update/user', (req, res) => {
    const {id, name, address, isActive, isRemoved, mobile, verticals} = req.body;

    if (util.appUtil.empty(id) || util.appUtil.empty(name)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }

    AgentSchema.findOneAndUpdate({
        _id: id
    }, {
        $set: {
            name: name,
            isActive: isActive,
            isRemoved: isRemoved,
            address: address,
            mobile: {
                number: parseInt(mobile.number),
                code: mobile.code
            },
            verticals: {
                name: verticals.name,
                _id: verticals._id
            }
        }
    }, {new: true}).exec((error, user) => {
        if (error) {
            if (error.code === 11000)
                return res.json(util.response.error(error, messageConf.error.duplicateEntry));
            return res.json(util.response.error(error, messageConf.error.error));
        } else {
            return res.json(util.response.success(user));
        }
    });
});
router.post('/removeUsers', (req, res) => {
    let token = req.decoded;
    let AdminId = token._id;
    const {ids} = req.body;
    if (util.appUtil.empty(ids)) {
        let error = messageConf.error.invalidParameter;
        return res.json(util.response.error(error, error));
    }
    AgentSchema.updateMany(
        {_id: {$in: ids}},
        {$set: {isRemoved: true}}, function (err) {
            if (err) return res.json(util.response.error(err, messageConf.error.internalError));
            return res.json(util.response.success());
        }
    );
});
router.post('/update/category', (req, res) => {
    const {_id, name, isActive, remove} = req.body;

    if (util.appUtil.empty(_id) || util.appUtil.empty(name) || util.appUtil.empty(isActive)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }
    let catname = name;
    if (remove) {
        catname = util.appUtil.generatePassword().toString();
    }
    CatSchema.findById(_id).exec((error, cat) => {
        if (error)
            return res.json(util.response.error(error, messageConf.error.invalidData));
        if (!cat)
            return res.json(util.response.error(messageConf.error.invalidData, messageConf.error.invalidData));
        cat.name = catname;
        cat.isActive = isActive;
        cat.save(error => {
            if (error) {
                if (error.code === 11000)
                    return res.json(util.response.error(error, messageConf.error.duplicateEntry));
                return res.json(util.response.error(error, messageConf.error.invalidData));
            }
            return res.json(util.response.success(cat));
        })
    });

});
router.post('/update/position', (req, res) => {
    const {catId, positionId, name, isActive, remove} = req.body;
    if (util.appUtil.empty(catId) || util.appUtil.empty(positionId) || util.appUtil.empty(name) || util.appUtil.empty(isActive)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }
    let catname = name;
    if (remove) {
        catname = util.appUtil.generatePassword().toString();
    }
    CatSchema.findById(catId).exec((error, cat) => {
        if (error)
            return res.json(util.response.error(error, messageConf.error.error));
        let subCat = cat.postions.id(positionId);
        if (!subCat) {
            return res.json(util.response.error(messageConf.error.invalidData, messageConf.error.invalidData));
        }
        subCat.name = catname;
        subCat.isActive = isActive;
        cat.save(err => {
            if (err) {
                return res.json(util.response.error(err, messageConf.error.internalError));
            } else {
                return res.json(util.response.success());
            }
        })
    });
});
router.post('/update/verticals', (req, res) => {
    const {_id, name, isActive, remove} = req.body;

    if (util.appUtil.empty(_id) || util.appUtil.empty(name) || util.appUtil.empty(isActive)) {
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));
    }
    let catname = name;
    if (remove) {
        catname = util.appUtil.generatePassword().toString();
    }
    VerticalSchema.findOneAndUpdate({
        _id: _id
    }, {
        $set: {
            name: catname,
            isActive: isActive
        }
    }, {new: true}).exec((err, vertical) => {
        if (err) {
            return res.json(util.response.error(err, messageConf.error.duplicateEntry));
        } else {
            return res.json(util.response.success(vertical));
        }
    });
});
router.get('/get/citiesStates', (req, res) => {
    CitySchema.find({}, (err, cityState) => {
        if (err)
            return res.json(util.response.error(err, messageConf.error.error));
        return res.json(util.response.success(cityState))
    })
});
router.post('/form/download', (req, res) => {
    let token = req.decoded;
    let admintId = token._id;
    AdminSchema.findById(admintId, (err, admin) => {
        if (err) {
            /* Error fetching admin */
            return res.json(util.response.error(err, messageConf.error.internalError))
        }
        if (!admin) {
            /* Invalid admin */
            return res.json(util.response.error(messageConf.error.invalidUser, messageConf.error.invalidUser));
        }
    });
});
router.post("/password/update", (req, res) => {
    const {oldPassword, newPassword} = req.body;
    if (util.appUtil.empty(oldPassword) || util.appUtil.empty(newPassword))
        return res.json(util.response.error(messageConf.error.invalidParameter, messageConf.error.invalidParameter));

    let userId = req.decoded._id;
    AdminSchema.findById(userId).exec((err, user) => {
        if (err) {
            return res.json(util.response.error(err, messageConf.error.error));
        }
        if (!user) {
            return res.json(util.response.error(messageConf.error.invalidData, messageConf.error.invalidData));
        }
        user.comparePassword(oldPassword, (err, isMatch) => {
            if (isMatch) {
                user.password = newPassword;
                user.save(err => {
                    if (err)
                        return res.json(util.response.error(err, messageConf.error.error));
                    return res.json(util.response.success());
                })
            } else {
                return res.json(util.response.error(messageConf.error.invalidData, messageConf.error.invalidData));
            }
        })
    })
});

module.exports = router;