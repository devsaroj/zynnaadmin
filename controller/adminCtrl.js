let AdminSchema = require('../db/model/Admin');
let AgentSchema = require('../db/model/Agent');
let FormSchema = require('../db/model/Forms');
let util = require('../util/util');
let messageConf = require('../constants/messages');
let mongoose = require('mongoose');
let Excel = require('exceljs');
let CategorySchema = require("../db/model/Category");
let VerticalSchema = require("../db/model/Verticals");


let authenticate = (username, password) => {
    return new Promise((resolve, reject) => {
        AdminSchema.findOne({
            userName: username.toLowerCase()
        }).select({
            name: 1,
            password: 1,
            email: 1
        }).limit(1).exec((err, admin) => {
            if (err) {
                return reject(err);
            }
            if (admin) {
                admin.comparePassword(password, (err, isMatch) => {
                    if (err) {
                        return reject(err);
                    }
                    if (isMatch) {
                        return resolve(admin.toJSON());
                    } else {
                        return reject("Incorrect Password");
                    }
                });
            } else {
                /* admin not found */
                return reject("invalid user");
            }

        })
    });
}

let getSubmittedForms = (limit, pageNo, search, skip, datefrom, dateto, filter) => {

    return new Promise((resolve, reject) => {
        search = search.toLowerCase().trim();
        let searchArray = search.split(' ');
        let regParam = searchArray.map((param) => {
            return new RegExp(param, 'i')
        });
        let dateType = 'custom'
        /*date types :- current,lm,l3m,l6m,custom*/
        let currentMonth = new Date();
        let from;
        let to = new Date();
        if (dateType == 'custom') {
            from = new Date(datefrom);
            to = new Date(dateto);
            to.setHours(0, 0, 0, 0);
            to.setDate(to.getDate() + 1);
            to = new Date(to)
        }

        let match = {};
        match['$match'] = {};
        let and = [
            {
                "createdAt": {
                    $gte: from,
                    $lte: to
                }
            },
            {
                "active": true
            },
            {
                $or: [
                    {"companyName": new RegExp(search, 'i')},
                    {"firstName": new RegExp(search, 'i')},
                    {"category.name": new RegExp(search, 'i')},
                    {"position.name": new RegExp(search, 'i')},
                    {"verticals.name": new RegExp(search, 'i')},
                    {"email": new RegExp(search, 'i')}
                ]
            }
        ];
        if (filter != null) {
            if (filter.category.length > 0) {
                and.push(
                    {"category._id": {"$in": filter.category.map(x => mongoose.Types.ObjectId(x))}}
                )
            }
            if (filter.verticals.length > 0) {
                and.push(
                    {"verticals._id": {"$in": filter.verticals.map(x => mongoose.Types.ObjectId(x))}}
                )
            }
            if (filter.users.length > 0) {
                and.push(
                    {"refId": {"$in": filter.users.map(x => mongoose.Types.ObjectId(x))}}
                )
            }
        }
        match['$match']['$and'] = and;
        getAllForms(and).then(count => {
                FormSchema.aggregate([
                    match,
                    {
                        $sort: {'updatedAt': -1}
                    },
                    {$skip: parseInt(skip)},
                    {$limit: parseInt(limit)},
                    {
                        $lookup: {
                            from: 'admins',
                            let: {"refId": "$refId"},
                            pipeline: [{
                                $match: {
                                    $expr: {
                                        $eq: ["$_id", "$$refId"]
                                    }
                                }
                            }, {
                                $project: {
                                    name: "$userName"
                                }
                            }],
                            as: 'adminJoins'
                        }
                    }, {
                        $lookup: {
                            from: 'agents',
                            let: {"refId": "$refId"},
                            pipeline: [{
                                $match: {
                                    $expr: {
                                        $eq: ["$_id", "$$refId"]
                                    }
                                }
                            }, {
                                $project: {
                                    name: 1
                                }
                            }],
                            as: 'agentsJoins'
                        }
                    }
                ]).exec(function (err, forms) {
                    if (err) return reject(err);
                    let results = [];
                    if (forms != null) {
                        results = forms;
                    }
                    return resolve({count: count, forms: results});
                });
            }
        );
    });

}
let createFile = (data, type, next) => {
    return new Promise((resolve, reject) => {
        let workbook = new Excel.Workbook();
        workbook.creator = 'Admin';
        workbook.created = new Date();
        workbook.views = [
            {
                x: 0, y: 0, width: 10000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ];
        let worksheet = workbook.addWorksheet('Forms');
        if (type == 'xlsx') {
            worksheet.columns = [
                {header: 'COMPANY NAME', key: 'companyName', width: 5},
                {header: 'TITLE', key: 'title', width: 5},
                {header: 'SURNAME', key: 'surname', width: 15},
                {header: 'NAME', key: 'firstName', width: 5},
                {header: 'CATEGORY', key: 'category', width: 5},
                {header: 'Type', key: 'type', width: 5},
                {header: 'POSITION', key: 'position', width: 5},
                {header: 'ADDRESS', key: 'addressLine', width: 5},
                {header: 'COUNTRY', key: 'country', width: 5},
                {header: 'STATE', key: 'state', width: 5},
                {header: 'CITY', key: 'city', width: 5},
                {header: 'PINCODE', key: 'pincode', width: 5},
                {header: 'ALTERNATIVE EMAIL ADDRESS 1', key: 'email', width: 5},
                {header: 'ALTERNATIVE EMAIL ADDRESS 1', key: 'email1', width: 5},
                {header: 'ALTERNATIVE EMAIL ADDRESS 2', key: 'email2', width: 5},
                {header: 'MOBILE NO.', key: 'mobile', width: 5},
                {header: 'ATERNATE MOBILE NO.', key: 'alNumber', width: 5},
                {header: 'LANDLINE NO.', key: 'landline', width: 5},
                {header: 'WEBSITE', key: 'website', width: 5},
                {header: 'VERTICAL', key: 'verticals', width: 5}
            ];
            data.map((form) => {
                let tempRows = {};
                if (form.companyName != null && form.companyName != '') {
                    tempRows.companyName = form.companyName.toUpperCase();
                }
                if (form.title != null && form.title != '') {
                    tempRows.title = form.title.toUpperCase();
                }
                if (form.surname != null && form.surname != '') {
                    tempRows.surname = form.surname.toUpperCase();
                }
                if (form.firstName != null && form.firstName != '') {
                    tempRows.firstName = form.firstName.toUpperCase();
                }
                if (typeof form.category != "undefined" && form.category.name != null && form.category.name != '') {
                    tempRows.category = form.category.name.toUpperCase();
                }
                if (form.type != null && form.type != '') {
                    tempRows.type = form.type.toUpperCase();
                }
                if (typeof form.position != "undefined" && form.position.name != null && form.position.name != '') {
                    tempRows.position = form.position.name.toUpperCase();
                }
                if (typeof form.address != "undefined" && form.address.addressLine != null && form.address.addressLine != '') {
                    tempRows.addressLine = form.address.addressLine.toUpperCase();
                }
                if (typeof form.address != "undefined" && form.address.country != null && form.address.country != '') {
                    tempRows.country = form.address.country.toUpperCase();
                }
                if (typeof form.address != "undefined" && form.address.state != null && form.address.state != '') {
                    tempRows.state = form.address.state.toUpperCase();
                }
                if (typeof form.address != "undefined" && form.address.city != null && form.address.city != '') {
                    tempRows.city = form.address.city.toUpperCase();
                }
                if (typeof form.address != "undefined" && form.address.pincode != null && form.address.pincode != '') {
                    tempRows.pincode = form.address.pincode;
                }
                if (form.email != null && form.email != '') {
                    tempRows.email = form.email.toUpperCase();
                }
                if (form.email1 != null && form.email1 != '') {
                    tempRows.email1 = form.email1.toUpperCase();
                }
                if (form.email2 != null && form.email2 != '') {
                    tempRows.email2 = form.email2.toUpperCase();
                }
                if (form.mobile.number != null && form.mobile.number != '') {
                    tempRows.mobile = form.mobile.code + ' ' + form.mobile.number;
                }
                if (form.alNumber.number != null && form.alNumber.number != '') {
                    tempRows.alNumber = form.alNumber.code + ' ' + form.alNumber.number;
                }
                if (form.landLine != null && form.landLine != '') {
                    tempRows.landline = form.landLine;
                }
                if (form.website != null && form.website != '') {
                    tempRows.website = form.website.toUpperCase();
                }
                if (typeof form.verticals != "undefined" && form.verticals.name != null && form.verticals.name != '') {
                    tempRows.verticals = form.verticals.name.toUpperCase();
                }
                worksheet.addRow(tempRows);
            });
        } else {
            /*label*/
            worksheet.columns = [
                {header: '', key: 'main', width: 25}]

            data.map((form) => {
                worksheet.addRow({main: form.companyName ? form.companyName.toUpperCase() : '-'});
                worksheet.addRow({main: ((form.firstName ? form.firstName.toUpperCase() + ' ' : '') + (form.surname ? form.surname.toUpperCase() + ' ' : ''))});
                worksheet.addRow({main: form.address.addressLine ? form.address.addressLine.toUpperCase() : '-'});
                let cityPin = ((form.address.city ? form.address.city.toUpperCase() : '') + '-' + (form.address.pincode ? form.address.pincode : ''));
                worksheet.addRow({main: cityPin});
                worksheet.addRow({main: form.mobile.number ? form.mobile.number : '-'});
                worksheet.addRow({main: ''});
                worksheet.addRow({main: ''});
            });
        }

        let FileName = new Date().getTime() + Math.ceil(Math.random() * 1000) + '.xlsx';
        workbook.xlsx.writeFile(FileName).then(function (x) {
            return resolve(FileName)
        }).catch((err) => {
            return reject(err)
        });
    });
}
let bulkUploadForm = (filePath) => {
    return new Promise((resolve, reject) => {
        Promise.all([getCategory(), getVerticals()]).then(x => {
            const [category, vertical] = x;

            let invalidRows = [];
            let validRows = [];

            let workbook = new Excel.Workbook();
            workbook.xlsx.readFile(filePath)
                .then(function () {
                    workbook.eachSheet(function (worksheet) {
                        let header = null;
                        worksheet.eachRow(function (row) {
                            if (header == null) {
                                header = row.values;
                            } else {
                                let newRow = {};
                                for (let index = 0; index < row.values.length; index++) {
                                    newRow[header[index]] = row.values[index];
                                }
                                if (typeof newRow['CATEGORY'] != "undefined" && newRow['CATEGORY'] != null && newRow['CATEGORY'] != '') {
                                    let matchedCategory = category.find(x => {
                                        if (x.name.toLowerCase() === newRow['CATEGORY'].toLowerCase()) {
                                            return x
                                        }
                                    });
                                    if (matchedCategory == null) {
                                        newRow['error'] = "Invalid Category";
                                        invalidRows.push(newRow);
                                        return true;
                                    } else {
                                        if (typeof newRow['POSITION'] != "undefined" && newRow['POSITION'] != null && newRow['POSITION'] != '') {
                                            let matchedPosition = matchedCategory.postions.find(x => {

                                                if (x.name.toLowerCase() === newRow['POSITION'].toLowerCase()) {
                                                    return x
                                                }
                                            });
                                            if (matchedPosition == null) {
                                                newRow['error'] = "Invalid Position";
                                                invalidRows.push(newRow);
                                                return true;
                                            }
                                        }
                                    }
                                }
                                let matchedVertical = vertical.find(x => {
                                    let tmpVertical = ''
                                    if (typeof newRow['VERTICAL'] != "undefined" && newRow['VERTICAL'] != null && newRow['VERTICAL'] != '') {
                                        if (x.name.toLowerCase() === newRow['VERTICAL'].toLowerCase()) {
                                            return x
                                        }
                                    }
                                });
                                if (matchedVertical == null) {
                                    newRow['error'] = "Invalid Vertical";
                                    invalidRows.push(newRow);
                                    return true;
                                } else {
                                    /* here add data to data base  */
                                    validRows.push(newRow)
                                }
                            }

                        });
                        if (invalidRows.length > 0) {
                            return reject({
                                invalidRows: invalidRows,
                                validRows: validRows
                            });
                        } else {
                            return resolve(validRows);
                        }
                    });
                });


        }).catch(error => {
            return reject(error)
        });
    });

}
let getAllForms = async (filter) => {
    let and = {
        '$and': filter
    }
    return await FormSchema.find(and).count().exec();
}
let getAllActiveForms = async () => {
    return await FormSchema.find({'active': true}).count().exec();
}
let getFilterVerticals = async () => {
    return await FormSchema.aggregate(
        [
            {"$match": {"active": true}},
            {$group: {_id: "$verticals._id", name: {$first: "$verticals.name"}}},
            {
                $project: {
                    _id: 1,
                    name: 1
                }
            }
        ]
    ).exec();
}

function getCategory() {
    return new Promise((resolve, reject) => {
        CategorySchema.find({isActive: true}).populate({
            path: "postions",
            match: {
                isActive: true
            }
        }).exec((err, category) => {
            if (err) {
                reject(err);
            }
            resolve(category);
        })
    });
}
function getVerticals() {
    return new Promise((resolve, reject) => {
        VerticalSchema.find({isActive: true}).exec((err, vertical) => {
            if (err) {
                reject(err);
            }
            resolve(vertical);
        })
    });
}
let addForm = async (data, refId) => {
    try {
        let category = await getCategory();
        let categoryMap = new Map();
        let positionMap = new Map();
        let verticalMap = new Map();

        for (let cat of category) {
            categoryMap.set(cat.name.toLowerCase(), cat._id);
            if (cat.postions && Array.isArray(cat.postions)) {
                for (let pos of cat.postions) {
                    positionMap.set(pos.name.toLowerCase(), pos._id)
                }
            }
        }
        let vertical = await  getVerticals();
        for (let vert of vertical) {
            verticalMap.set(vert.name.toLowerCase(), vert._id);
        }

        let bulkData = data.map(x => {
            let cat = null;
            let pos = null;
            let vertical = null;
            let mobile = {
                number: '',
                code: 91
            };
            let email = null;
            let email1 = null;
            let email2 = null;
            if (x['CATEGORY'] && x['CATEGORY'].toString().trim() !== '') {
                cat = {
                    name: x['CATEGORY'].toLowerCase(),
                    _id: categoryMap.get(x['CATEGORY'].toLowerCase())
                }
            }
            if (x['POSITION'] && x['POSITION'].toString().trim() !== '') {
                pos = {
                    name: x['POSITION'].toLowerCase(),
                    _id: positionMap.get(x['POSITION'].toLowerCase())
                }
            }
            if (x['VERTICAL'] && x['VERTICAL'].toString().trim() !== '') {
                vertical = {
                    name: x['VERTICAL'].toLowerCase(),
                    _id: verticalMap.get(x['VERTICAL'].toLowerCase())
                }
            }

            if (x['MOBILE NO.'] && x['MOBILE NO.'].toString().trim() !== '') {
                mobile = {
                    number: x['MOBILE NO.'],
                    code: x['MOBILE COUNTRY CODE'] || '91'
                }
            }
            if (x['PRIMARY EMAIL ADDRESS'] && x['PRIMARY EMAIL ADDRESS'].text && x['PRIMARY EMAIL ADDRESS'].text.toString().trim() !== '') {
                email = x['PRIMARY EMAIL ADDRESS'].text;
            }
            if (x['ALTERNATIVE EMAIL ADDRESS 1'] && x['ALTERNATIVE EMAIL ADDRESS 1'].text && x['ALTERNATIVE EMAIL ADDRESS 1'].text.toString().trim() !== '') {
                email1 = x['ALTERNATIVE EMAIL ADDRESS 1'].text;
            }
            if (x['ALTERNATIVE EMAIL ADDRESS 2'] && x['ALTERNATIVE EMAIL ADDRESS 2'].text && x['ALTERNATIVE EMAIL ADDRESS 2'].text.toString().trim() !== '') {
                email2 = x['ALTERNATIVE EMAIL ADDRESS 2'].text;
            }
            return {
                refId: refId,
                formNumber: (new Date().getTime()).toPrecision(),
                companyName: x['COMPANY NAME'],
                title: x['TITLE'],
                surname: x['SURNAME'],
                firstName: x['NAME'],
                category: cat,
                position: pos,
                verticals: vertical,
                address: {
                    addressLine: x['ADDRESS'] || '',
                    city: x['CITY'] || '',
                    state: x['STATE'] || '',
                    country: x['COUNTRY'] || '',
                    pincode: x['PINCODE'] || '',
                },
                mobile: mobile,
                alNumber: {
                    number: '',
                    code: 91
                },
                landLine: x['LANDLINE NO.'],
                website: util.appUtil.validateObject(x['WEBSITE']),
                type: 'prospective',
                email: email,
                email1: email1,
                email2: email2,
                active: true
            }

        });
        return await FormSchema.insertMany(bulkData);
    } catch (er) {
        throw new Error(er);
    }
}
let createInvalidFormsFile = (data, invalidfileFullPath, next) => {
    return new Promise((resolve, reject) => {
        let workbook = new Excel.Workbook();
        workbook.creator = 'Admin';
        workbook.created = new Date();
        workbook.views = [
            {
                x: 0, y: 0, width: 10000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ];
        let worksheet = workbook.addWorksheet('Forms');
        worksheet.columns = [
            {header: 'COMPANY NAME', key: 'companyName', width: 5},
            {header: 'TITLE', key: 'title', width: 5},
            {header: 'SURNAME', key: 'surname', width: 15},
            {header: 'NAME', key: 'firstName', width: 5},
            {header: 'CATEGORY', key: 'category', width: 5},
            {header: 'POSITION', key: 'position', width: 5},
            {header: 'ADDRESS', key: 'addressLine', width: 5},
            {header: 'COUNTRY', key: 'country', width: 5},
            {header: 'STATE', key: 'state', width: 5},
            {header: 'CITY', key: 'city', width: 5},
            {header: 'PINCODE', key: 'pincode', width: 5},
            {header: 'ALTERNATIVE EMAIL ADDRESS 1', key: 'email', width: 5},
            {header: 'ALTERNATIVE EMAIL ADDRESS 1', key: 'email1', width: 5},
            {header: 'ALTERNATIVE EMAIL ADDRESS 2', key: 'email2', width: 5},
            {header: 'MOBILE COUNTRY CODE', key: 'mobilecountrycode', width: 5},
            {header: 'MOBILE NO.', key: 'mobile', width: 5},
            {header: 'LANDLINE NO.', key: 'landline', width: 5},
            {header: 'WEBSITE', key: 'website', width: 5},
            {header: 'VERTICAL', key: 'verticals', width: 5},
            {header: 'ERROR', key: 'error', width: 5},
        ];
        let i = 1;
        data.map((x) => {

            let email = null;
            let email1 = null;
            let email2 = null;
            if (x['PRIMARY EMAIL ADDRESS'] && x['PRIMARY EMAIL ADDRESS'].text && x['PRIMARY EMAIL ADDRESS'].text.toString().trim() !== '') {
                email = x['PRIMARY EMAIL ADDRESS'].text;
            }
            if (x['ALTERNATIVE EMAIL ADDRESS 1'] && x['ALTERNATIVE EMAIL ADDRESS 1'].text && x['ALTERNATIVE EMAIL ADDRESS 1'].text.toString().trim() !== '') {
                email1 = x['ALTERNATIVE EMAIL ADDRESS 1'].text;
            }
            if (x['ALTERNATIVE EMAIL ADDRESS 2'] && x['ALTERNATIVE EMAIL ADDRESS 2'].text && x['ALTERNATIVE EMAIL ADDRESS 2'].text.toString().trim() !== '') {
                email2 = x['ALTERNATIVE EMAIL ADDRESS 2'].text;
            }
            let tempRows = {};
            if (x['COMPANY NAME'] != null && x['COMPANY NAME'] != '') {
                tempRows.companyName = x['COMPANY NAME'].toUpperCase();
            }
            if (x['TITLE'] != null && x['TITLE'] != '') {
                tempRows.title = x['TITLE'].toUpperCase();
            }
            if (x['SURNAME'] != null && x['SURNAME'] != '') {
                tempRows.surname = x['SURNAME'].toUpperCase();
            }
            if (x['NAME'] != null && x['NAME'] != '') {
                tempRows.firstName = x['NAME'].toUpperCase();
            }
            if (x['CATEGORY'] != null && x['CATEGORY'] != '') {
                tempRows.category = x['CATEGORY'].toUpperCase();
            }

            if (x['POSITION'] != null && x['POSITION'] != '') {
                tempRows.position = x['POSITION'].toUpperCase();
            }
            i++;
            if (x['ADDRESS'] != null && x['ADDRESS'] != '') {
                tempRows.addressLine = x['ADDRESS'].toUpperCase();
            }
            if (x['COUNTRY'] != null && x['COUNTRY'] != '') {
                tempRows.country = x['COUNTRY'].toUpperCase();
            }
            if (x['STATE'] != null && x['STATE'] != '') {
                tempRows.state = x['STATE'].toUpperCase();
            }
            if (x['CITY'] != null && x['CITY'] != '') {
                tempRows.city = x['CITY'].toUpperCase();
            }
            if (x['PINCODE'] != null && x['PINCODE'] != '') {
                tempRows.pincode = x['PINCODE'];
            }
            tempRows.email = email;
            tempRows.email1 = email1;
            tempRows.email2 = email2;
            if (x['MOBILE NO.'] != null && x['MOBILE NO.'] != '') {
                tempRows.mobile = x['MOBILE NO.'];
            }
            if (x['MOBILE COUNTRY CODE'] != null && x['MOBILE COUNTRY CODE'] != '') {
                tempRows.mobilecountrycode = x['MOBILE COUNTRY CODE'];
            }
            if (x['LANDLINE NO.'] != null && x['LANDLINE NO.'] != '') {
                tempRows.landline = x['LANDLINE NO.'];
            }
            tempRows.website = util.appUtil.validateObject(x['WEBSITE']);

            if (x['VERTICAL'] != null && x['VERTICAL'] != '') {
                tempRows.verticals = x['VERTICAL'].toUpperCase();
            }
            tempRows.error = x['error'].toUpperCase();
            worksheet.addRow(tempRows);
        });

        let FileName = new Date().getTime() + Math.ceil(Math.random() * 1000) + '.xlsx';
        let fullpathtosave = `${invalidfileFullPath}${FileName}`;
        workbook.xlsx.writeFile(fullpathtosave).then(function (x) {
            return resolve(FileName)
        }).catch((err) => {
            return reject(err)
        });
    });
}
let getDashboardCardsData = () => {
    return new Promise((resolve, reject) => {
        getAllActiveForms().then(count => {
            FormSchema.aggregate(
                [
                    {"$match": {"active": true}},
                    {$group: {_id: "$category.name", total: {$sum: 1}}},
                    {
                        $project: {
                            catName: "$_id",
                            _id: 0,
                            total: 1
                        }
                    }
                ]
            ).exec(function (err, catGroup) {
                if (err) return reject(err);
                let results = [];
                if (catGroup != null) {
                    results = catGroup;
                }
                return resolve({count: count, categories: results});
            });
        });
    });
}
let getFilterCategoriesAndVerticals = () => {
    return new Promise((resolve, reject) => {
        getFilterVerticals().then(verticals => {
            FormSchema.aggregate(
                [
                    {"$match": {"active": true}},
                    {$group: {_id: "$category._id", name: {$first: "$category.name"}}},
                    {
                        $project: {
                            _id: 1,
                            name: 1
                        }
                    }
                ]
            ).exec(function (err, catGroup) {
                if (err) return reject(err);
                let results = [];
                if (catGroup != null) {
                    results = catGroup;
                }
                return resolve({verticals: verticals, categories: results});
            });
        });
    });
}

let getAllSubmittedForms = (limit, pageNo, search, skip, datefrom, dateto) => {

    return new Promise((resolve, reject) => {
        search = search.toLowerCase().trim();
        let searchArray = search.split(' ');
        let regParam = searchArray.map((param) => {
            return new RegExp(param, 'i')
        });
        let dateType = 'custom'
        /*date types :- current,lm,l3m,l6m,custom*/
        let currentMonth = new Date();
        let from;
        let to = new Date();
        if (dateType == 'custom') {
            from = new Date(datefrom);
            to = new Date(dateto);
            to.setHours(0, 0, 0, 0);
            to.setDate(to.getDate() + 1);
            to = new Date(to)
        }
        FormSchema.aggregate([
            {
                $match: {
                    $and: [
                        {
                            "createdAt": {
                                $gte: from,
                                $lte: to
                            }
                        },
                        {
                            "active": true
                        },
                        {
                            $or: [
                                {"companyName": new RegExp(search, 'i')},
                                {"firstName": new RegExp(search, 'i')},
                                {"category.name": new RegExp(search, 'i')},
                                {"position.name": new RegExp(search, 'i')},
                                {"verticals.name": new RegExp(search, 'i')},
                                {"email": new RegExp(search, 'i')}
                            ]
                        }
                    ]
                }
            }
        ]).exec(function (err, forms) {
            if (err) return reject(err);
            let results = [];
            if (forms != null) {
                results = forms;
            }
            return resolve(results);
        });
    });

}
module.exports = {
    authenticate: authenticate,
    getSubmittedForms: getSubmittedForms,
    createFile: createFile,
    bulkUploadForm: bulkUploadForm,
    addForm: addForm,
    createInvalidFormsFile: createInvalidFormsFile,
    getDashboardCardsData: getDashboardCardsData,
    getAllSubmittedForms: getAllSubmittedForms,
    getFilterCategoriesAndVerticals: getFilterCategoriesAndVerticals
};