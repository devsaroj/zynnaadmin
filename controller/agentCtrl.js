let AgentSchema = require('../db/model/Agent');
let util = require('../util/util');
let messageConf = require('../constants/messages');
let mongoose = require('mongoose');
let CategorySchema = require("../db/model/Category");
let VerticalSchema = require("../db/model/Verticals");
let FormSchema = require("../db/model/Forms");

let authenticate = (username, password) => {
    return new Promise((resolve, reject) => {
        AgentSchema.findOne({
            email: username.toLowerCase(),
            isActive: true,
            isRemoved: false
        }).select({
            name: 1,
            password: 1,
            email: 1
        }).limit(1).exec((err, admin) => {
            if (err) {
                return reject(err);
            }
            if (admin) {
                admin.comparePassword(password, (err, isMatch) => {
                    if (err) {
                        return reject(err);
                    }
                    if (isMatch) {
                        return resolve(admin.toJSON());
                    } else {
                        return reject("Incorrect Password");
                    }
                });
            } else {
                /* admin not found */
                return reject("invalid user");
            }

        })
    });
}
let addForm = (data, refId) => {
    return new Promise((resolve, reject) => {
        Promise.all([getCategory(), getVerticals()]).then(x => {
            const [category, vertical] = x;
            let formSchema = new FormSchema();
            formSchema.refId = refId;
            formSchema.email = data.email;
            formSchema.email1 = data.email1;
            formSchema.email2 = data.email2;
            formSchema.type = data.type;
            formSchema.title = data.title;
            formSchema.address = {
                addressLine: data.address.addressLine,
                pincode: data.address.pincode,
                city: data.address.city,
                state: data.address.state,
                country: data.address.country
            };
            formSchema.companyName = data.companyName;
            formSchema.firstName = data.firstName;
            formSchema.surname = data.surname;
            formSchema.formNumber = parseInt(data.formNumber);
            if (data.category != null && data.category != '') {
                let matchedCategory = category.find(x => (x._id) == data.category);
                if (matchedCategory == null) {
                    return reject('Invalid Category');
                } else {
                    formSchema.category = {
                        _id: matchedCategory._id,
                        name: matchedCategory.name
                    };
                    if (data.position != null && data.position != '') {
                        if (typeof matchedCategory.postions != "undefined") {
                            let matchedPosition = matchedCategory.postions.find(x => (x._id) == data.position);
                            if (matchedPosition == null) {
                                return reject('Invalid Position');
                            } else {
                                formSchema.position = {
                                    _id: matchedPosition._id,
                                    name: matchedPosition.name
                                };
                            }
                        } else {
                            formSchema.position = '';
                        }
                    } else {
                        formSchema.position = '';
                    }
                }
            } else {
                formSchema.category = '';
            }

            if (data.verticals != null && data.verticals != '') {
                let matchedverticals = vertical.find(x => (x._id) == data.verticals);
                if (matchedverticals == null) {
                    return reject('Invalid Verticals');
                } else {
                    formSchema.verticals = {
                        _id: matchedverticals._id,
                        name: matchedverticals.name
                    };
                }
            } else {
                return reject('Please select a vertical');
            }
            formSchema.active = true;
            if (data.mobile.number != null && data.mobile.number != '') {
                formSchema.mobile = {
                    number: parseInt(data.mobile.number),
                    code: data.mobile.code
                };
            } else {
                formSchema.mobile = ''
            }
            if (data.alNumber.number != null && data.alNumber.number != '') {
                formSchema.alNumber = {
                    number: parseInt(data.alNumber.number),
                    code: data.alNumber.code
                };
            } else {
                formSchema.alNumber = ''
            }

            formSchema.landLine = parseInt(data.landLine) || '';
            formSchema.website = data.website;
            formSchema.type = data.type;
            formSchema.save((err, lastFormObj) => {
                if (err) return reject(err);
                return resolve(lastFormObj);
            });
        }).catch(error => {
            return reject(error)
        });
    });
}
function getCategory() {
    return new Promise((resolve, reject) => {
        CategorySchema.find({isActive: true}).populate({
            path: "postions",
            match: {
                isActive: true
            }
        }).exec((err, category) => {
            if (err) {
                reject(err);
            }
            resolve(category);
        })
    });
}

function getVerticals() {
    return new Promise((resolve, reject) => {
        VerticalSchema.find({isActive: true}).exec((err, vertical) => {
            if (err) {
                reject(err);
            }
            resolve(vertical);
        })
    });
}
module.exports = {
    authenticate: authenticate,
    addForm: addForm,
};