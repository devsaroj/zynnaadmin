let mongoose = require('mongoose');

module.exports = (function (catSchema, verticalSchema) {
    let getLatestCategory = function (updateDate = null, isActiveCheck = null, cb) {
        let $sort = {
            "$sort": {
                "updatedAt": -1
            }
        };
        let $dateMatch = {
            "$match": {
                "updatedAt": {
                    $gt: new Date(updateDate)
                }
            }
        };
        let aggregate = [];
        if (isActiveCheck != null) {
            aggregate.push({
                "$match": {
                    "isActive": isActiveCheck
                }
            })
        }
        aggregate.push($sort);
        if (updateDate) {
            /* Get all categories after update Date */
            aggregate.push($dateMatch);
        }
        catSchema.aggregate(aggregate).exec(cb);
    };
    let getLatestVertical = function (updateDate = null, isActiveCheck = null, cb) {
        let $sort = {
            "$sort": {
                "updatedAt": -1
            }
        };
        let $dateMatch = {
            "$match": {
                "updatedAt": {
                    $gt: new Date(updateDate)
                }
            }
        };
        let aggregate = [];
        if (isActiveCheck != null) {
            aggregate.push({
                "$match": {
                    "isActive": isActiveCheck
                }
            })
        }
        aggregate.push($sort);
        if (updateDate) {
            /* Get all categories after update Date */
            aggregate.push($dateMatch);
        }
        verticalSchema.aggregate(aggregate).exec(cb);
    };
    return {
        getLatestCategory: getLatestCategory,
        getLatestVertical: getLatestVertical,
    }
})(require('../db/model/Category'), require('../db/model/Verticals'));

