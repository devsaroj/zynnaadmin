let mongoConnection = {
    // host: "192.168.1.108",
    // host: "192.168.1.47",
    host: "localhost",
    port: "27017",
    db: "zynnadb",
    user: "",
    password: "",
    getConnectionUrl: function () {
        let user = this.user || "";
        let password = this.password || "";
        if (user === "" || password === "") {
            return "mongodb://" + this.host + ":" + this.port + "/" + this.db;
        } else {
            return "mongodb://" + this.user + ":" + this.password + "@" + this.host + ":" + this.port + "/" + this.db;
        }
    }
};
let SALT_WORK_FACTOR = 10;
let tokenCode = "4wHnu0J8MD";

module.exports = {
    db: mongoConnection,
    salt: SALT_WORK_FACTOR,
    tokenCode: tokenCode,
};
